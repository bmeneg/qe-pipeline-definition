#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

if [[ -z "${BEAKER_JOBIDS:-}" ]]; then
    echo "FAIL: BEAKER_JOBIDS is not set"
    exit 0
fi
echo "Waiting for beaker jobs to finish: ${BEAKER_JOBIDS}"

if [[ -z "${DW_CHECKOUT}" ]]; then
    echo "FAIL: DW_CHECKOUT is not set"
    exit 1
fi

JOB_NOTIFY=${JOB_NOTIFY:-${JOB_OWNER}}

TEST_JOB_TIMEOUT_HOURS=${TEST_JOB_TIMEOUT_HOURS:-24}

# wait for 24hrs for jobs to complete
wait_start=$(date +%s)
max_time=$(( TEST_JOB_TIMEOUT_HOURS*3600 ))
# in case of failures connecting to beaker `bkr job-watch` might fail
# we should retry it and continue waiting for up to 24hrs since original
# start time.
# don't fail script if bkr job-watch doesn't return 0 as this is normal if some task doesn't succeed
set +e
# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
while [[ $(( $(date +%s) - max_time )) -lt "${wait_start}" ]]; do
    timeout $(( $(date +%s) - max_time )) bkr job-watch "${BEAKER_JOBIDS}"
    if [[ $? == 124 ]]; then
        echo "WARN: tests took over ${TEST_JOB_TIMEOUT_HOURS} hours to run, give up waiting for completion..."
        bkr job-cancel "${BEAKER_JOBIDS}"
        break
    fi

    bkr job-results "${BEAKER_JOBIDS}" > job.xml
    job_status=$(sed -n 's/.*status="\([a-zA-Z]\+\)".*/\1/p' < job.xml | tail -1)
    if [[ "${job_status}" == "Completed" ]] ||
       [[ "${job_status}" == "Aborted" ]] ||
       [[ "${job_status}" == "Cancelled" ]]; then
           break
    fi
done

set -xe
echo "reporting results of beaker jobs ${BEAKER_JOBIDS}"
compose=$(sed -n 's/.*distro="\([a-zA-Z0-9.-]\+\)".*/\1/p' < job.xml | head -1)
if [[ -z "${compose}" ]]; then
    echo "FAIL: couldn't find beaker compose used by job: ${BEAKER_JOBIDS}"
    exit 1
fi

if [[ -n "${TEST_PACKAGE_NVR:-}" ]]; then
    if [[ -z "${TEST_SOURCE_PACKAGE_NVR}" ]]; then
        echo "FAIL: TEST_PACKAGE_NVR is set as ${TEST_PACKAGE_NVR}, but TEST_SOURCE_PACKAGE_NVR is not set."
        exit 1
    fi
    # it is expected if TEST_PACKAGE_NVR is set that TEST_SOURCE_PACKAGE_NVR is also set
    src_pkg_nvr="${TEST_SOURCE_PACKAGE_NVR}"
    pkg_nvr="${TEST_PACKAGE_NVR}"
else
    src_pkg_nvr=$(find_compose_pkg -c "${compose}" -p "${TEST_SOURCE_PACKAGE_NAME:?}")
    if [[ -z "${src_pkg_nvr}" ]]; then
        echo "FAIL: couldn't find nvr for ${TEST_PACKAGE_NAME:?} on compose ${compose}"
        exit 1
    fi
    # set the packge nvr based on nvr of package source nvr
    # shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
    pkg_nvr=$(echo "${src_pkg_nvr}" | sed "s/${TEST_SOURCE_PACKAGE_NAME}/${TEST_PACKAGE_NAME:?}/")
fi
# allow DW_CHECKOUT to contain command line execution
DW_CHECKOUT=$(eval echo "${DW_CHECKOUT}")
contacts=()
for contact in ${JOB_NOTIFY}; do
    contacts+=(--contact "${contact}"@redhat.com)
done

kcidb_tool create --source beaker --src-nvr "${src_pkg_nvr}" --nvr "${pkg_nvr}" "${contacts[@]}" -i job.xml -c "\"${DW_CHECKOUT}\"" -o output.json --tests-provisioner-url "${CI_JOB_URL:?}"
kcidb_tool push2umb -i output.json --certificate /tmp/umb_certificate.pem
