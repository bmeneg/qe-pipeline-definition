#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

if [[ "${DRY_RUN:-}" == "1" ]]; then
    exit 0
fi

set -x
# remove the / that are passed with TEST_PACKAGE_NAME
TEST_PACKAGE_NAME_ARCH="${TEST_PACKAGE_NAME_ARCH//\/}"
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_PACKAGE_NAME=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\1/g')
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_ARCH=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\2/g')
ARCH_PARAM=""
if [[ "${TEST_ARCH}" != "allarches" ]]; then
    ARCH_PARAM="--arch ${TEST_ARCH}"
    WOW_WHITEBOARD="${WOW_WHITEBOARD} - ${TEST_ARCH}"
fi

if [[ -z "${BKR_COMPOSE_TAGS}" ]]; then
    echo "Fail: BKR_COMPOSE_TAGS is not set."
    exit 1
fi

tags="--tag ${BKR_COMPOSE_TAGS// / --tag }"
# shellcheck disable=SC2086 # Double quote to prevent globbing
bkr distros-list --name "${BKR_COMPOSE:?}" ${tags} --limit 1 | tee compose.txt
compose=$(grep "Name:" compose.txt | awk '{print$2}')
if [[ -z "${compose}" ]]; then
    echo "FAIL: Couldn't query beaker compose."
    exit 1
fi
# don't fail pipeline if a command fails
set +e
if [[ -n "${TEST_PACKAGE_NVR:-}" ]]; then
    pkg_nvr="${TEST_PACKAGE_NVR}"
else
    pkg_nvr=$(find_compose_pkg -c "${compose}" -p "${TEST_PACKAGE_NAME}")
fi
if [[ -z "${pkg_nvr}" ]]; then
    echo "FAIL: couldn't find nvr for ${TEST_PACKAGE_NAME} on compose ${compose}"
    exit 1
fi

# TODO: how to install an specific kernel using wow?

# allow WOW_WHITEBOARD to contain command line execution
WOW_WHITEBOARD=$(eval echo "${WOW_WHITEBOARD}")

# shellcheck disable=SC2086 # Double quote to prevent globbing
bkr workflow-tomorrow --distro "${compose}" ${ARCH_PARAM} --ks-meta=redhat_ca_cert --exact --task-file "${WOW_TASKFILE:?}" --job-owner="${JOB_OWNER:?}" --whiteboard "${WOW_WHITEBOARD}" 2>&1 | tee bkr_job.txt
BKR_JOBID=$(grep -Eo 'TJ#[[:digit:]]+$' bkr_job.txt | sed 's/TJ#/J:/')
if [[ -z "${BKR_JOBID}" ]]; then
    echo "FAIL: Couldn't submit beaker job."
    exit 1
fi
{
    echo BEAKER_JOBIDS="${BKR_JOBID}"
    echo TEST_PACKAGE_NAME="${TEST_PACKAGE_NAME}"
    echo TEST_SOURCE_PACKAGE_NAME="${TEST_SOURCE_PACKAGE_NAME:?}"
    echo TEST_ARCH="${TEST_ARCH}"
} > test_variables.env
