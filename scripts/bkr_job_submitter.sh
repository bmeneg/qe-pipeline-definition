#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

if [[ "${DRY_RUN:-}" == "1" ]]; then
    exit 0
fi

set -x
# remove the / that are passed with TEST_PACKAGE_NAME
TEST_PACKAGE_NAME_ARCH="${TEST_PACKAGE_NAME_ARCH//\/}"
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_PACKAGE_NAME=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\1/g')
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_ARCH=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\2/g')
if [[ "${TEST_ARCH}" != "allarches" ]]; then
    JOB_SUBMITTER_PARMS="${JOB_SUBMITTER_PARMS} --arch ${TEST_ARCH}"
fi
if [[ -n "${TEST_PLAN_REPO:-}" ]]; then
    git clone --depth 1 "${TEST_PLAN_REPO}" test-plan
    cp -r test-plan/"${TEST_PLAN_PATH:?}" ~/
    rm -rf test-plan
fi

if [[ -z "${BKR_COMPOSE_TAGS}" ]]; then
    echo "Fail: BKR_COMPOSE_TAGS is not set."
    exit 1
fi

tags="--tag ${BKR_COMPOSE_TAGS// / --tag }"
# shellcheck disable=SC2086 # Double quote to prevent globbing
bkr distros-list --name "${BKR_COMPOSE:?}" ${tags} --limit 1 | tee compose.txt
BUILD=$(grep "Name:" compose.txt | awk '{print$2}')
if [[ -z "${BUILD}" ]]; then
    echo "FAIL: couldn't find beaker compose to use"
    exit 1
fi
export BUILD
if [[ -n "${TEST_PACKAGE_NVR:-}" ]]; then
    pkg_nvr="${TEST_PACKAGE_NVR}"
else
    pkg_nvr=$(find_compose_pkg -c "${BUILD}" -p "${TEST_SOURCE_PACKAGE_NAME:?}")
fi
if [[ -z "${pkg_nvr}" ]]; then
    echo "FAIL: couldn't find nvr for ${TEST_PACKAGE_NAME} on compose ${BUILD}"
    exit 1
fi
JOB_SUBMITTER_PARMS="${JOB_SUBMITTER_PARMS} --nvr ${pkg_nvr}"

# allow JOB_SUBMITTER_PARMS to contain command line execution
JOB_SUBMITTER_PARMS=$(eval echo "${JOB_SUBMITTER_PARMS}")

# shellcheck disable=SC2086 # Double quote to prevent globbing
JobSubmitter.sh "${TEST_PLAN_NAME:?}" ${JOB_SUBMITTER_PARMS} 2>&1 | tee bkr_job.txt
BKR_JOBID=$(grep -Eo 'TJ#[[:digit:]]+$' bkr_job.txt | sed 's/TJ#/J:/')
if [[ -z "${BKR_JOBID}" ]]; then
    echo "FAIL: Couldn't submit beaker job."
    exit 1
fi
{
    echo BEAKER_JOBIDS="${BKR_JOBID}"
    echo TEST_PACKAGE_NAME="${TEST_PACKAGE_NAME}"
    echo TEST_SOURCE_PACKAGE_NAME="${TEST_SOURCE_PACKAGE_NAME}"
    echo TEST_ARCH="${TEST_ARCH}"
} > test_variables.env
