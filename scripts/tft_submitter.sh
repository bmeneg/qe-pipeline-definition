#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

if [[ "${DRY_RUN:-}" == "1" ]]; then
    exit 0
fi

set -x
# remove the / that are passed with TEST_PACKAGE_NAME
TEST_PACKAGE_NAME_ARCH="${TEST_PACKAGE_NAME_ARCH//\/}"
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_PACKAGE_NAME=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\1/g')
# shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
TEST_ARCH=$(echo "${TEST_PACKAGE_NAME_ARCH}" | sed 's/\(.*\)-\(.*\)/\2/g')
tft_params=("--compose ${TFT_COMPOSE}" "--arch ${TEST_ARCH}"
            "--git-url ${TFT_PLAN_URL}" "--plan ${TFT_PLAN}")
if [[ -n "${TFT_CONTEXT:-}" ]]; then
    tft_params+=("--context ${TFT_CONTEXT}")
fi

# shellcheck disable=SC2086,SC2048 # Double quote to prevent globbing - Use "$@" (with quotes) to prevent whitespace problems
testing-farm request --no-wait --kickstart "kernel-options=enforcing=0" --kickstart "kernel-options-post=enforcing=0 rd.emergency=reboot" \
    ${tft_params[*]} | tee request_output.txt
REQUEST_ID=$(grep "api" request_output.txt | grep -o '[^/]*$')
if [[ -z "${REQUEST_ID}" ]]; then
    echo "FAIL: Couldn't submit testing-farm request."
    exit 1
fi

{
    echo TFT_REQUEST_ID="${REQUEST_ID}"
    echo TEST_PACKAGE_NAME="${TEST_PACKAGE_NAME}"
    echo TEST_SOURCE_PACKAGE_NAME="${TEST_SOURCE_PACKAGE_NAME:?}"
    echo TEST_ARCH="${TEST_ARCH}"
} > test_variables.env
