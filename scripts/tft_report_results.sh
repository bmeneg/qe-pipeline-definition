#!/usr/bin/bash
set -Eeuo pipefail
shopt -s inherit_errexit

if [[ -z "${TFT_REQUEST_ID:-}" ]]; then
    echo "FAIL: TFT_REQUEST_ID is not set"
    exit 0
fi
echo "Waiting for testing farm to finish with ${TFT_REQUEST_ID}"

JOB_NOTIFY=${JOB_NOTIFY:-${JOB_OWNER}}

# don't fail pipeline if a command fails
set +e
# wait for 24hrs for jobs to complete
wait_start=$(date +%s)
max_time=$(( 24*3600 ))
# in case of failures connecting to beaker `bkr job-watch` might fail
# we should retry it and continue waiting for up to 24hrs since original
# start time.
# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
while [[ $(( $(date +%s) - max_time )) -lt "${wait_start}" ]]; do
    timeout $(( $(date +%s) - max_time )) testing-farm watch --id "${TFT_REQUEST_ID}"
    if [[ $? == 124 ]]; then
        echo "WARN: tests took over 24hr to run, give up waiting for completion..."
        testing-farm cancel "${REQUEST_ID:?}"
        break
    fi

    # make sure watch returned, because request completed and not due some infra problem
    curl --retry 50 "${TFT_API_URL:?}"/requests/"${TFT_REQUEST_ID:?}" > request_status.json
    state=$(jq -r ".state" < request_status.json)
    if [[ "${state}" == "complete" ]] ||
       [[ "${state}" == "error" ]]; then
        break
    fi
done
set -xe
echo "reporting results of testing farm request ${REQUEST_ID}"
xunit_url=$(jq -r '.result.xunit_url' < request_status.json)
if [[ -z "${xunit_url}" ]]; then
    echo "FAIL: couldn't get xunit url"
    exit 1
fi

if [[ "${xunit_url}" == "null" ]]; then
    echo "FAIL: xunit url is null, likely an ifra errror. There is nothing to report to DataWarehouse"
    exit 1
fi

# wait some time for testing farm to create the results.xml
sleep 60
curl --retry 50 "${xunit_url}" > results.xml

compose=$(sed -n 's/.*compose.*value="\([a-zA-Z0-9.-]\+\)".*/\1/p' < results.xml | head -1)
if [[ -z "${compose}" ]]; then
    echo "FAIL: couldn't find testing farm compose used by: ${REQUEST_ID}"
    exit 1
fi

# workaround to get the real compose
# $ https://issues.redhat.com/browse/TFT-790
curl --retry 50 "${TFT_OSCI_STORAGE:?}/composes-production.json" > composes-production.json
compose=$(jq -r '[."SYMBOLIC_COMPOSES"[] | select(."'"${compose}"'")][0] | ."'"${compose}"'"' < composes-production.json)

if [[ -z "${compose}" ]]; then
    echo "FAIL: couldn't find testing farm real compose used by: ${REQUEST_ID}"
    exit 1
fi

if [[ -n "${TEST_PACKAGE_NVR:-}" ]]; then
    if [[ -z "${TEST_SOURCE_PACKAGE_NVR}" ]]; then
        echo "FAIL: TEST_PACKAGE_NVR is set as ${TEST_PACKAGE_NVR}, but TEST_SOURCE_PACKAGE_NVR is not set."
        exit 1
    fi
    # it is expected if TEST_PACKAGE_NVR is set that TEST_SOURCE_PACKAGE_NVR is also set
    src_pkg_nvr="${TEST_SOURCE_PACKAGE_NVR}"
    pkg_nvr="${TEST_PACKAGE_NVR}"
else
    src_pkg_nvr=$(find_compose_pkg -c "${compose}" -p "${TEST_SOURCE_PACKAGE_NAME:?}")
    if [[ -z "${src_pkg_nvr}" ]]; then
        echo "FAIL: couldn't find nvr for ${TEST_PACKAGE_NAME:?} on compose ${compose}"
        exit 1
    fi
    # set the packge nvr based on nvr of package source nvr
    # shellcheck disable=SC2001 # See if you can use ${variable//search/replace} instead.
    pkg_nvr=$(echo "${src_pkg_nvr}" | sed "s/${TEST_SOURCE_PACKAGE_NAME}/${TEST_PACKAGE_NAME}/")
fi
# allow DW_CHECKOUT to contain command line execution
DW_CHECKOUT=$(eval echo "${DW_CHECKOUT}")
contacts=()
for contact in ${JOB_NOTIFY}; do
    contacts+=(--contact "${contact}"@redhat.com)
done

kcidb_tool create --source testing-farm --src-nvr "${src_pkg_nvr}" --nvr "${pkg_nvr}" "${contacts[@]}" -i results.xml -c "\"${DW_CHECKOUT}\"" -o output.json --tests-provisioner-url "${CI_JOB_URL:?}"
kcidb_tool push2umb -i output.json --certificate /tmp/umb_certificate.pem
