#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'bkr_report_results'
    cleanup() {
        rm -rf rm job.xml
    }
    AfterEach 'cleanup'

    export TEST_SOURCE_PACKAGE_NAME=kernel
    export TEST_PACKAGE_NAME=kernel-rt
    export BEAKER_JOBIDS=J:123456
    export JOB_OWNER=test
    export CI_JOB_URL=test_url
    # shellcheck disable=SC2016 # Expressions don't expand in single quotes, use double quotes for that.
    export DW_CHECKOUT='dw checkout: $(cmd)'
    export mocked_cmd="Wed May 22"

    Mock cmd
        echo "${mocked_cmd}"
    End

    Mock bkr
        echo 'status="Completed"'
        echo 'distro="mocked-distro"'
        echo 'status="Completed"'
        echo 'distro="mocked-distro"'
        echo 'status="Completed"'
        echo 'distro="mocked-distro"'
    End

    Mock find_compose_pkg
        echo "kernel-5.14.0.mock.el10"
    End

    Mock kcidb_tool
        echo "kcidb_tool $*"
    End

    It 'can report results - with only JOB_OWNER'
        When run script scripts/bkr_report_results.sh
        The status should be success
        The stdout should include "kcidb_tool create --source beaker --src-nvr kernel-5.14.0.mock.el10 --nvr kernel-rt-5.14.0.mock.el10 --contact test@redhat.com -i job.xml -c \"dw checkout: ${mocked_cmd}\" -o output.json --tests-provisioner-url test_url"
        The stdout should include "kcidb_tool push2umb -i output.json --certificate /tmp/umb_certificate.pem"
        The stderr should not equal ""
    End

    It 'can report results - with JOB_NOTIFY'
        export JOB_NOTIFY="test1 test2"
        When run script scripts/bkr_report_results.sh
        The status should be success
        The stdout should include "kcidb_tool create --source beaker --src-nvr kernel-5.14.0.mock.el10 --nvr kernel-rt-5.14.0.mock.el10 --contact test1@redhat.com --contact test2@redhat.com -i job.xml -c \"dw checkout: ${mocked_cmd}\" -o output.json --tests-provisioner-url test_url"
        The stdout should include "kcidb_tool push2umb -i output.json --certificate /tmp/umb_certificate.pem"
        The stderr should not equal ""
    End
End
