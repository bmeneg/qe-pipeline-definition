#!/usr/bin/bash

# shellcheck disable=SC2317 # Command appears to be unreachable

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'tft_report'
    export JOB_NOTIFY=test
    export TEST_PACKAGE_NAME_ARCH=kernel-debug-x86_64
    export TEST_PACKAGE_NAME=kernel-debug
    export TEST_SOURCE_PACKAGE_NAME=kernel
    export TFT_REQUEST_ID=mocked_compose
    export TFT_API_URL=test-api
    export TFT_OSCI_STORAGE=osci-storage
    export REQUEST_ID=mocked-tft-id
    # shellcheck disable=SC2016 # Expressions don't expand in single quotes, use double quotes for that.
    export DW_CHECKOUT='dw checkout: $(cmd)'
    export mocked_cmd="Wed May 22"
    export CI_JOB_URL=test_url
    export xunit_result="https://mock.com"

    cleanup() {
        rm -rf results.xml
    }
    After 'cleanup'


    Mock sleep
        exit 0
    End

    Mock cmd
        echo "${mocked_cmd}"
    End

    Mock testing-farm
        exit 0
    End

    Mock find_compose_pkg
        echo "kernel-5.14.0.mock.el10"
    End

    Mock kcidb_tool
        echo "kcidb_tool $*"
    End

    Mock curl
        if [[ "$*" =~ "requests" ]]; then
            echo "{\"state\":\"complete\", \"result\":{\"xunit_url\":\"${xunit_result}\"}}"
        elif [[ "$*" =~ https://mock.com ]]; then
            echo '<property name="compose" value="RHEL-9.5.0-Nightly"/>'
            echo '<property name="compose" value="RHEL-9.5.0-Nightly"/>'
            echo '<property name="compose" value="RHEL-9.5.0-Nightly"/>'
        elif [[ "$*" =~ composes-production.json ]]; then
            echo '{"SYMBOLIC_COMPOSES": [{ "RHEL-9.5.0-Nightly": "RHEL-9.5.0-20240528.55"}]}'
        else
            echo "$@"
        fi
    End

    It 'can report results'
        When run script scripts/tft_report_results.sh
        The status should be success
        The stdout should include "Waiting for testing farm to finish with mocked_compose"
        The stdout should include "reporting results of testing farm request"
        The stdout should include "kcidb_tool create --source testing-farm --src-nvr kernel-5.14.0.mock.el10 --nvr kernel-debug-5.14.0.mock.el10 --contact test@redhat.com -i results.xml -c \"dw checkout: Wed May 22\" -o output.json --tests-provisioner-url test_url"
        The stdout should include "kcidb_tool push2umb -i output.json --certificate /tmp/umb_certificate.pem"
        The stderr should include "curl --retry 50 https://mock.com"
        The stderr should include "find_compose_pkg -c RHEL-9.5.0-20240528.55 -p kernel"
    End

    It 'can exit with failure if xunit_url is null'
        export xunit_result="null"
        When run script scripts/tft_report_results.sh
        The status should be failure
        The stdout should include "Waiting for testing farm to finish with mocked_compose"
        The stdout should include "reporting results of testing farm request"
        The stdout should include "FAIL: xunit url is null, likely an ifra errror. There is nothing to report to DataWarehouse"
        The stderr should not equal ""
    End
End
