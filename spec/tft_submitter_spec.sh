#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'tft_submitter'
    cleanup() {
        rm -rf test_variables.env
    }
    AfterEach 'cleanup'

    export TEST_PACKAGE_NAME_ARCH=kernel-debug-x86_64
    export TEST_SOURCE_PACKAGE_NAME=kernel
    export TFT_COMPOSE=mocked_compose
    export TFT_PLAN_URL=mocked_plan_url
    export TFT_PLAN=mocked_testplan


    Mock testing-farm
        echo "api https://api.tft-mocked/v0.1/requests/mocked-request-id"
    End

    It 'can submit job'
        When run script scripts/tft_submitter.sh
        The status should be success
        The stdout should include "api "
        The stderr should include "testing-farm request --no-wait --kickstart kernel-options=enforcing=0 --kickstart 'kernel-options-post=enforcing=0 rd.emergency=reboot' --compose mocked_compose --arch x86_64 --git-url mocked_plan_url --plan mocked_testplan"
        The contents of file test_variables.env should include "TFT_REQUEST_ID=mocked-request-id"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job with context'
        export TFT_CONTEXT='test-context'
        When run script scripts/tft_submitter.sh
        The status should be success
        The stdout should include "api "
        The stderr should include "testing-farm request --no-wait --kickstart kernel-options=enforcing=0 --kickstart 'kernel-options-post=enforcing=0 rd.emergency=reboot' --compose mocked_compose --arch x86_64 --git-url mocked_plan_url --plan mocked_testplan --context ${TFT_CONTEXT}"
        The contents of file test_variables.env should include "TFT_REQUEST_ID=mocked-request-id"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End
End
