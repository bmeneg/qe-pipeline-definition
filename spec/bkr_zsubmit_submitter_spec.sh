#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'bkr_zsubmit_submitter'
    startup() {
        mkdir -p zsubmit-repo/Sustaining
    }

    cleanup() {
        rm -f test_variables.env
        rm -f bkr_ids.txt
        rm -rf Sustaining
    }
    BeforeEach 'startup'
    AfterEach 'cleanup'

    export TEST_PACKAGE_NAME_ARCH=kernel-debug-x86_64
    export TEST_SOURCE_PACKAGE_NAME=kernel
    export BKR_COMPOSE="mocked_compose"
    export BKR_COMPOSE_TAGS="mock-tag1 mock-tag2"
    export ZSUBMIT_REPO=mock-repo
    export ZSUBMIT_TIER='KT1 KT2 REG'
    # shellcheck disable=SC2016 # Expressions don't expand in single quotes, use double quotes for that.
    export ZSUBMIT_PARAMS='--noduplicate --hotconfig --whiteboard $(cmd)'
    export bkr_output="Name: mocked_compose"


    Mock cmd
        echo "mycmd"
    End

    Mock git
        echo "git $*"
    End

    Mock bkr
        echo "${bkr_output}"
    End

    Mock find_compose_pkg
        echo "kernel-5.14.0-mock.el10.x86_64"
    End

    Mock pip3
        echo "pip3 $*"
    End

    Mock python3
        echo "python3 $*"
        if [[ "$1" == "ZSubmit.py" ]]; then
            echo -n "J:123456" >> bkr_ids.txt
        elif [[ "$1" == "ZAddon.py" ]]; then
            echo -n " J:123457" >> bkr_ids.txt
        else
            echo "unsupported file"
            exit 1
        fi
    End

    It 'can submit job - without ZAddon'
        When run script scripts/bkr_zsubmit_submitter.sh
        The status should be success
        The stdout should include "${bkr_output}"
        The stdout should include "python3 ZSubmit.py --noduplicate --hotconfig --whiteboard mycmd --arch x86_64 --nvr kernel-5.14.0-mock.el10.x86_64 --tier \"KT1 KT2 REG\" --idf bkr_ids.txt"
        The stdout should not include "python3 ZAddon.py"
        The stderr should include "bkr distros-list --name mocked_compose --tag mock-tag1 --tag mock-tag2 --limit 1"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job - with ZAddon and ZADDON_PARAMS is empty string'
        export ZADDON_PARAMS=""
        When run script scripts/bkr_zsubmit_submitter.sh
        The status should be success
        The stdout should include "${bkr_output}"
        The stdout should include "python3 ZSubmit.py --noduplicate --hotconfig --whiteboard mycmd --arch x86_64 --nvr kernel-5.14.0-mock.el10.x86_64 --tier \"KT1 KT2 REG\" --idf bkr_ids.txt"
        The stdout should include "python3 ZAddon.py  --nvr kernel-5.14.0-mock.el10.x86_64 --idf bkr_ids.txt"
        The stderr should include "bkr distros-list --name mocked_compose --tag mock-tag1 --tag mock-tag2 --limit 1"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456 J:123457"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End

    It 'can submit job - with ZAddon and ZADDON_PARAMS is not empty'
        export ZADDON_PARAMS="--more-params"
        When run script scripts/bkr_zsubmit_submitter.sh
        The status should be success
        The stdout should include "${bkr_output}"
        The stdout should include "python3 ZSubmit.py --noduplicate --hotconfig --whiteboard mycmd --arch x86_64 --nvr kernel-5.14.0-mock.el10.x86_64 --tier \"KT1 KT2 REG\" --idf bkr_ids.txt"
        The stdout should include "python3 ZAddon.py --more-params --nvr kernel-5.14.0-mock.el10.x86_64 --idf bkr_ids.txt"
        The stderr should include "bkr distros-list --name mocked_compose --tag mock-tag1 --tag mock-tag2 --limit 1"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456 J:123457"
    End
End
