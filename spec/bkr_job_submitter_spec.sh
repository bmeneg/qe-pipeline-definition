#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'bkr_job_submitter'
    cleanup() {
        rm -rf test_variables.env
    }
    AfterEach 'cleanup'

    export TEST_PACKAGE_NAME_ARCH=kernel-debug-x86_64
    export TEST_SOURCE_PACKAGE_NAME=kernel
    export TEST_PLAN_NAME=mock-testplan
    # shellcheck disable=SC2016 # Expressions don't expand in single quotes, use double quotes for that.
    export JOB_SUBMITTER_PARMS='--job-owner=test $(cmd)'
    export BKR_COMPOSE=mock-bkr-compose
    export BKR_COMPOSE_TAGS="mock-tag1 mock-tag2"
    export bkr_output="Name: mocked_compose"
    export JobSubmitter_output="TJ#123456"


    Mock cmd
        echo "mycmd"
    End

    Mock bkr
        echo "${bkr_output}"
    End

    Mock find_compose_pkg
        echo "kernel-5.14.0-mock.el10.x86_64"
    End

    Mock JobSubmitter.sh
        if [[ ${BUILD:?} != "mocked_compose" ]]; then
            echo "JobSubmitter.sh expects BUILD variable to be set to beaker compose"
            exit 1
        fi
        echo "${JobSubmitter_output}"
    End

    It 'can submit job'
        When run script scripts/bkr_job_submitter.sh
        The status should be success
        The stdout should include "${bkr_output}"
        The stdout should include "${JobSubmitter_output}"
        The stderr should include "bkr distros-list --name mock-bkr-compose --tag mock-tag1 --tag mock-tag2 --limit 1"
        The stderr should include "JobSubmitter.sh mock-testplan --job-owner=test mycmd --arch x86_64 --nvr kernel-5.14.0-mock.el10.x86_64"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=J:123456"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End
End
