#!/usr/bin/bash

# shellcheck disable=SC2312 # Consider invoking this command separately to avoid masking its return value
eval "$(shellspec - -c) exit 1"

Describe 'stbkr_submitter'
    startup() {
        mkdir -p kernel/storage/misc/toolbox
    }
    cleanup() {
        rm -rf test_variables.env
        rm -rf kernel
    }
    Before 'startup'
    AfterEach 'cleanup'

    export TEST_PACKAGE_NAME_ARCH=kernel-debug-x86_64
    export TEST_SOURCE_PACKAGE_NAME=kernel
    export STBKR_TESTS_CONF_REPO="mock_repo"
    export BKR_COMPOSE="mock_compose"
    export BKR_COMPOSE_TAG="mock_tag"
    export CONFIG="test_config"
    export STBKR_GROUP="test_group"
    export JOB_OWNER=test

    export stbkr_output="J:123456"

    Mock git
        echo "git $*"
    End

    Mock stbkr
        echo "${stbkr_output}"
    End

    It 'can submit job'
        When run script scripts/stbkr_submitter.sh
        The status should be success
        The stdout should include "git clone --depth 1 mock_repo kernel"
        The stderr should include "stbkr --default-kernel 0 --debug-kernel 1 -r mock_compose --dist_tag mock_tag -c test_conf/test_config -g test_group -a x86_64 --job-owner test"
        The contents of file test_variables.env should include "BEAKER_JOBIDS=${stbkr_output}"
        The contents of file test_variables.env should include "TEST_PACKAGE_NAME=kernel-debug"
        The contents of file test_variables.env should include "TEST_SOURCE_PACKAGE_NAME=kernel"
        The contents of file test_variables.env should include "TEST_ARCH=x86_64"
    End
End
